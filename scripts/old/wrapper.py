import os


class Wrapper(object):
    def __init__(self, config):
        self.config = config
        self.reinit()

    def reinit(self):
        self.lengthBounds = self.config.get('lengthBounds') or [0, 300]
        self.largestStructBounds = self.config.get('largestStructBounds') or [0, 100]
        self.totalConnectionsBounds = self.config.get('totalConnectionsBounds') or [0, 1000]
        self.length = self.config.get('length') or 4
        self.dataDir = self.config['dataDir']
        self.nbStages = self.config['nbStages']
        self._createDomains()

    def _createDomains(self):
        self.domainsStr = self.config.get('domains') or ["TGGGT", "GAGTC"]
        # TODO : create domain classes ?


    def _removeTmpFiles(self, fileList):
        keepTemporaryFiles = self.config.get('keepTemporaryFiles')
        if not keepTemporaryFiles:
            for f in fileList:
                try:
                    os.remove(f)
                except:
                    pass

    def _evalFn(self, instanceId, expeId, stageId):
        res = {'length': 0, 'largestStruct': 0, 'totalConnections': 0}
        # Also call _removeTmpFiles on temporary files ...
        return res

    def _normaliseResults(self, res, keys):
        def normalise(key):
            minVal, maxVal = self.__dict__[key+"Bounds"]
            res[key] = max(minVal, min(maxVal, res[key]))
        list(map(normalise, keys))
        return res

    # TODO
    def evaluateGenotype(self, instanceId, expeId):
        stageId = 0 # XXX
        res = self._evalFn(instanceId, expeId, stageId)
        res = self._normaliseResults(res, ["length", "largestStruct", "totalConnections"])
        return res




# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
