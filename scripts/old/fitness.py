#!/usr/bin/env python3

#import math
import numpy as np
import os



########### ANALYSIS ########### {{{1

class StageAnalyser(object):
    def __init__(self, expeId, stageId, dataDir):
        self.expeId = expeId
        self.stageId = stageId
        self.dataDir = dataDir
        self.reinit()

    def reinit(self):
        self.energyFilename = os.path.join(self.dataDir, "energy%i_%i.dat" % (self.expeId, self.stageId))
        self.topologyFilename = os.path.join(self.dataDir, "topology%i_%i" % (self.expeId, self.stageId))
        self._extractEnergyData()
        self._extractTopologyData()

    def _extractEnergyData(self):
        pass # TODO

    def _extractTopologyData(self):
        pass # TODO


    def compareTotalEnergy(self, other):
        return 0.42 # TODO

#    def compareEnergyWrtTime(self, other):
#        return 0.42

    def compareNbStrands(self, other):
        return 0.42 # TODO

#    def compareOccupation(self, other):
#        return 0.42



class DNAStructAnalyser(object):
    def __init__(self, nbStages, expeId, dataDir):
        self.nbStages = nbStages
        self.expeId = expeId
        self.dataDir = dataDir
        self.reinit()

    def reinit(self):
        self.stage0 = StageAnalyser(self.expeId, 0, self.dataDir)
        self.stageN = StageAnalyser(self.expeId, self.nbStages - 1, self.dataDir)
        self.fitness = float("-inf")
        self.features = np.full(4, float("-inf"))

    def _computeFeatures(self):
        self.totEnergyScore = self.stage0.compareTotalEnergy(self.stageN)
        #self.energyWrtTimeScore = self.stage0.compareEnergyWrtTime(self.stageN)
        self.nbStrandsScore = self.stage0.compareNbStrands(self.stageN)
        #self.occupationScore = self.stage0.compareOccupation(self.stageN)
        #self.features = [self.totEnergyScore, self.energyWrtTimeScore, self.nbStrandsScore, self.occupationScore]
        self.features = [self.totEnergyScore, self.nbStrandsScore]

    def _computeFitness(self):
        #self.fitness = np.mean([self.totEnergyScore, self.energyWrtTimeScore, self.nbStrandsScore, self.occupationScore])
        self.fitness = np.mean([self.totEnergyScore, self.nbStrandsScore])

    def getScores(self):
        self._computeFeatures()
        self._computeFitness()
        return [self.fitness] + self.features




########### MAIN ########### {{{1
#if __name__ == "__main__":
#    #import argparse
#    #parser = argparse.ArgumentParser()
#    #parser.add_argument('--testType', type=str, default='map-elites', help="Type of test (map-elites, cvt-map-elites)")
#    #args = parser.parse_args()
#    pass



# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
