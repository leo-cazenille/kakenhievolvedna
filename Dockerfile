FROM lcazenille/ubuntupysshgcc

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Tokyo

RUN \
    apt-get update && \
    apt-get install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -yqq graphviz git \
    libtiff5-dev libjpeg-turbo8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev

RUN mkdir -p /home/user

# Download repositories
RUN cd /home/user && git clone https://bitbucket.org/leo-cazenille/kakenhievolvedna.git

RUN pip install --upgrade cython && pip3 install --upgrade cython

# Install peppercorn
RUN \
    pip install mpmath==0.19 numpy scipy sympy==1.2 networkx matplotlib==2.2.4 && \
    pip3 install tqdm colorama psutil parse powerlaw networkx matplotlib==2.2.4 seaborn graphviz pydot && \
    pip3 install -U qdpy==0.1.0 && \
    git clone https://github.com/leo-cazenille/peppercornenumerator.git && \
    cd /peppercornenumerator && python2 setup.py install
#RUN \
#    pip3 install -U -r /home/user/kakenhievolvedna/requirements.txt


# Install nupack
COPY nupack3.2.2.tar.gz /
RUN \
    tar xvf nupack3.2.2.tar.gz && \
    cd nupack3.2.2 && \
    mkdir build && cd build && \
    cmake .. && make -j 20 && \
    make install


# Prepare for entrypoint execution
#CMD ["/bin/bash"]
ENTRYPOINT ["/home/user/kakenhievolvedna/entrypoint.sh"]
#CMD ["1000", "normal"]

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
